#!/usr/bin/perl

use strict;

use Mojo::UserAgent;

my $letter = $ARGV[0];

unless ( $letter =~ /^[WNES]$/ ) {
	print "Supported parameters: W / N / E / S\n";
	exit;
}

my $ua = Mojo::UserAgent->new;

$ua->connect_timeout(60);
$ua->inactivity_timeout(60);

$ua = $ua->cookie_jar( Mojo::UserAgent::CookieJar->new );

$ua->get('http://app.toronto.ca/DevelopmentApplications/dwr/test/MapSearchService');

my $msg = $ua->get('http://app.toronto.ca/DevelopmentApplications/dwr/engine.js')->res->content->asset->get_chunk;

my ($session) = ( $msg =~ /dwr\.engine\._origScriptSessionId = "([^"]+)";/ );

#print "session: $session\n";

my $body = <<"EOF";
callCount=1
page=/DevelopmentApplications/dwr/test/MapSearchService
httpSessionId=
scriptSessionId=${session}123
c0-scriptName=MapSearchService
c0-methodName=searchApplications
c0-id=0
c0-param0=string:$letter
c0-param1=string:
c0-param2=string:
c0-param3=string:
c0-param4=string:
c0-param5=string:
c0-param6=string:
c0-param7=string:
c0-param8=string:
c0-param9=string:
c0-param10=string:
c0-param11=string:
c0-param12=string:
c0-param13=string:
batchId=0
EOF

#print "$body\n";

my $url = 'http://app.toronto.ca/DevelopmentApplications/dwr/call/plaincall/MapSearchService.searchApplications.dwr';

#print $tx->res->to_string;

my $tx = $ua->post($url => { 'Accept' => '*/*' } => $body)
	->res->content->asset->move_to("rawjs-$letter.txt");
