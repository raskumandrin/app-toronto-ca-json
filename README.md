

```
./scraper.draft.pl N
./scraper.draft.pl S
./scraper.draft.pl W
./scraper.draft.pl E
```

This will generate files ```rawjs-N.txt```, ```rawjs-S.txt```, ```rawjs-W.txt```, ```rawjs-E.txt```.

```
./parser.draft.pl rawjs-N.txt > json-N.txt
./parser.draft.pl rawjs-S.txt > json-S.txt
./parser.draft.pl rawjs-W.txt > json-W.txt
./parser.draft.pl rawjs-E.txt > json-E.txt
```

```json-*.txt``` — are compact json files (oneliners)

```
./pretty.json.pl json-N.txt > pjson-N.txt
./pretty.json.pl json-S.txt > pjson-S.txt
./pretty.json.pl json-W.txt > pjson-W.txt
./pretty.json.pl json-E.txt > pjson-E.txt
```

```pjson-*.txt``` — are nested indented json files (for humans)
