#!/usr/bin/perl

use strict;

die `pod2text $0` if (scalar @ARGV) == 0;

use DBI;

use FindBin;
use lib "$FindBin::Bin/lib";

use JSON;
use Mojo::UserAgent;

# check available DBI drivers (we need Pg!)
# if no driver, install: sudo apt-get install libdbd-pg-perl
# print "$_\n" foreach ( DBI->available_drivers );

my $dbname   = $ARGV[1] || 'postgres';
my $host     = $ARGV[2] || 'localhost';
my $user     = $ARGV[3] || 'postgres';
my $password = $ARGV[4] || 'golosneba';

my $letter = $ARGV[0];

unless ( $letter =~ /^[WNES]$/ ) {
	print "Supported parameters: W / N / E / S\n";
	exit;
}

my $ua = Mojo::UserAgent->new;

$ua->connect_timeout(60);
$ua->inactivity_timeout(60);

$ua = $ua->cookie_jar( Mojo::UserAgent::CookieJar->new );

$ua->get('http://app.toronto.ca/DevelopmentApplications/dwr/test/MapSearchService');

my $msg = $ua->get('http://app.toronto.ca/DevelopmentApplications/dwr/engine.js')->res->content->asset->get_chunk;

my ($session) = ( $msg =~ /dwr\.engine\._origScriptSessionId = "([^"]+)";/ );

#print "session: $session\n";

my $body = <<"EOF";
callCount=1
page=/DevelopmentApplications/dwr/test/MapSearchService
httpSessionId=
scriptSessionId=${session}123
c0-scriptName=MapSearchService
c0-methodName=searchApplications
c0-id=0
c0-param0=string:$letter
c0-param1=string:
c0-param2=string:
c0-param3=string:
c0-param4=string:
c0-param5=string:
c0-param6=string:
c0-param7=string:
c0-param8=string:
c0-param9=string:
c0-param10=string:
c0-param11=string:
c0-param12=string:
c0-param13=string:
batchId=0
EOF

#print "$body\n";

my $url = 'http://app.toronto.ca/DevelopmentApplications/dwr/call/plaincall/MapSearchService.searchApplications.dwr';

#print $tx->res->to_string;


my $content = $ua->post($url => { 'Accept' => '*/*' } => $body)->res->content->asset->slurp;
	
# хранилище итогового списка
my @result;

# список переменных для итогового списка
# _remoteHandleCallback('0','0',[s0,s1, ... ,s215,s216]);
my ($var_list) = ( $content =~ /_remoteHandleCallback\('0','0',\[([^\]]+)\]\);/ );


sub get_prop_list {
	my ($item) = ( @_ );
	my @matches = ( $content =~ /$item\.([^=]+)=/g );
	return \@matches;
}

sub get_prop_val {
	my ($item,$prop) = ( @_ );
	my ( $val ) = ( $content =~ /$item\.$prop=([^;]+);/ );
	if ($val =~ /^s\d+$/) {
		# Определяем, хеш ли это или массив
		if ( $content =~ /var $val=\[\];/ ) {
			# массив
	
			# получаем массив элементов
			# s222[0]=s223;s222[1]=s224;s222[2]=s225;
			my ( @arr ) = ( $content =~ /$val\[\d+\]=(s\d+);/g );
			my @result_arr;
			foreach my $s (@arr) {
				push @result_arr,build_hash_ref($s);
			}
			$val = \@result_arr;
		}
		else {
			# хеш
			$val = build_hash_ref($val)
		}
	
	}
	if ($val eq 'null') {
		$val = undef;
	}
	if ($val =~ /^".*"$/ ) {
		( $val ) = ( $val =~ /^"(.*)"$/  );
	}
	return $val;
}

sub build_hash_ref {
	my ($s) = ( @_ );
	my $hash;
	my $props = get_prop_list($s);
	foreach my $prop (@$props) {
		$hash->{$prop} = get_prop_val($s,$prop);
	}
	return $hash;
}

foreach my $s ( split ',',$var_list ) {
	push @result,build_hash_ref($s);
	build_hash_ref($s);
}

if ( (scalar @ARGV) == 1 ) {
	# output to stdout
	print JSON->new->utf8(1)->pretty(1)->encode(\@result);
}
else {
	# output to database

	my $query = "INSERT INTO json_toronto_ca (jtc_type,jtc_data) VALUES (?,?);";

	my $dbh = DBI->connect("dbi:Pg:dbname=$dbname;host=$host",$user,$password,{AutoCommit=>1,RaiseError=>1,PrintError=>0});

	$dbh->begin_work();
	my $rv = $dbh->do($query,undef,$letter, JSON->new->utf8(1)->pretty(1)->encode(\@result) );
	$dbh->commit();
	if (!defined $rv) {
		print "Error executing query '$query': " . $dbh->errstr . "\n";
		exit(0);
	}

	$dbh->disconnect();

}



__END__

=head1 NAME

Get data from app.toronto.ca, transform it to JSON and print to STDOUT or store into PostgreSQL

=head1 SYNOPSYS

example run without parameters:

./json_toronto_ca.pl

example run with one parameters:

./json_toronto_ca.pl W > wjson.txt

(1) Type of query: W / N / E / S

Output pretty JSON to STDOUT (in this example to file wjson.txt)

example run with many parameters:

./json_toronto_ca.pl W dbname dbhost dbuser dbpassword

Parameters:
(1) Type of query: W / N / E / S
(2) Database name
(3) Database host
(4) Database user
(5) Database password

Output compact JSON to database

=head1 DESCRIPTION

Result will be stored into postgres table:

  CREATE TYPE enum_district AS ENUM ('W','E','S','N');
  CREATE TABLE public.json_toronto_ca (
     jtc_id serial, 
     jtc_timestamp timestamp without time zone DEFAULT now(), 
     jtc_type enum_district, 
     jtc_data text, 
     PRIMARY KEY (jtc_id)
  ) WITH ( OIDS = FALSE );

=head1 INSTALLATION

Driver for Postgres and Perl:

sudo apt-get install libdbd-pg-perl

Mojolicious library is already included locally

=head1 AUTHOR

Stas Raskumandrin <stas@raskumandrin.ru>, http://stas.raskumandrin.ru

=cut