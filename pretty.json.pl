#!/usr/bin/perl

use strict;

use FindBin;
use lib "$FindBin::Bin/lib";

use JSON;

undef $/;
my $content = <>;

my $json = JSON->new;
print $json->pretty->encode($json->decode($content));
