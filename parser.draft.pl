#!/usr/bin/perl

use strict;

use Mojo::JSON qw(decode_json encode_json);

undef $/;
my $content = <>;


# хранилище итогового списка
my @result;

# список переменных для итогового списка
# _remoteHandleCallback('0','0',[s0,s1, ... ,s215,s216]);
my ($var_list) = ( $content =~ /_remoteHandleCallback\('0','0',\[([^\]]+)\]\);/ );


sub get_prop_list {
	my ($item) = ( @_ );
	my @matches = ( $content =~ /$item\.([^=]+)=/g );
	return \@matches;
}

sub get_prop_val {
	my ($item,$prop) = ( @_ );
	my ( $val ) = ( $content =~ /$item\.$prop=([^;]+);/ );
	if ($val =~ /^s\d+$/) {
		# Определяем, хеш ли это или массив
		if ( $content =~ /var $val=\[\];/ ) {
			# массив
		
			# получаем массив элементов
			# s222[0]=s223;s222[1]=s224;s222[2]=s225;
			my ( @arr ) = ( $content =~ /$val\[\d+\]=(s\d+);/g );
			my @result_arr;
			foreach my $s (@arr) {
				push @result_arr,build_hash_ref($s);
			}
			$val = \@result_arr;
		}
		else {
			# хеш
			$val = build_hash_ref($val)
		}
		
	}
	if ($val eq 'null') {
		$val = undef;
	}
	if ($val =~ /^".*"$/ ) {
		( $val ) = ( $val =~ /^"(.*)"$/  );
	}
	return $val;
}

sub build_hash_ref {
	my ($s) = ( @_ );
	my $hash;
	my $props = get_prop_list($s);
	foreach my $prop (@$props) {
		$hash->{$prop} = get_prop_val($s,$prop);
	}
	return $hash;
}

foreach my $s ( split ',',$var_list ) {
	push @result,build_hash_ref($s);
	build_hash_ref($s);
}

print encode_json(\@result);
